vec2 rand2(in vec2 co) {
    return vec2(
            fract(sin(dot(co.xy, vec2(-0.129898,0.78233))) * 43.7585453),
            fract(sin(dot(co.xy, vec2(0.129898,0.22188))) * 71.8617489));
}
float r(in float f) {
    return fract(sin(dot(vec2(f, sin(f)), vec2(-0.129898,0.78233))) * 43.7585453);
}
vec4 lava(vec3 pos) {
    float x = {{time}} + {{audio}}[1];
    float off =
        0.1*sin(dot(pos.xz, vec2(5., 11.)) + 1.5*x) + 
        0.1*sin(dot(pos.xz, vec2(-11.2, 1.2)) - 1.2*x) + 
        0.1*sin(dot(pos.xz, vec2(0., -5.)) + 1.3*x) + 
        0.1*sin(dot(pos.xz, vec2(-12., -1.2)) - 1.9*x);
    return vec4(0.5 + off, 0.1, 0., max(length(pos.xz) - 1., pos.y + 0.2*off - 1.9));
}
vec4 spheres(vec3 pos) {
    vec3 p = pos - vec3(0., 60., 0);
    float res = 100.;
    for (int i=0; i<10; i++) {
        res = min(res, length(p - vec3((30. + 3.*{{audio}}[0])*(rand2(vec2(0.1*sin(0.1*{{time}} + float(i)))) - 0.5), 0.)) - 1. - 2.*r(float(i)) + 0.5*{{audio}}[1]);
    }
    return vec4(1., {{audio}}[0], 0., res);
}

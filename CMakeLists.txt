cmake_minimum_required(VERSION 3.1)

project(darkisland)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR})
option(FULLSCREEN "Fulscreen." OFF)
option(MUTE "Mute audio." OFF)

include_directories(${CMAKE_BINARY_DIR})
add_custom_command(OUTPUT ${CMAKE_BINARY_DIR}/frag.glsl
    COMMAND demo-tool shader ${CMAKE_CURRENT_SOURCE_DIR}/src/scene.json -o ${CMAKE_BINARY_DIR}/frag.glsl -r -t standalone
    WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
    COMMENT Generating shader.
    DEPENDS src/scene.json src/scene_before.glsl
)
add_custom_target(shader DEPENDS ${CMAKE_BINARY_DIR}/frag.glsl)
add_custom_command(OUTPUT ${CMAKE_BINARY_DIR}/audio.h
    COMMAND demo-tool audio ${CMAKE_CURRENT_SOURCE_DIR}/src/scene.mid -o ${CMAKE_BINARY_DIR}/audio.h
    COMMENT Generating audio.
    DEPENDS src/scene.mid
)
add_custom_target(audio DEPENDS ${CMAKE_BINARY_DIR}/audio.h)

if(FULLSCREEN)
    add_definitions(-DFULLSCREEN)
endif()
if(MUTE)
    add_definitions(-DMUTE)
endif()

add_subdirectory(demo-template)
add_dependencies(${PROJECT_NAME} shader audio)
